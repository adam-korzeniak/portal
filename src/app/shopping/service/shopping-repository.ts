import {Injectable} from '@angular/core';
import {ILocation} from '../model/ILocation';
import {IProduct} from '../model/IProduct';


@Injectable({providedIn: 'root'})
export class ShoppingRepository {

  products: IProduct[];
  locations: ILocation[];

  public getLocations(): ILocation[] {
    return this.locations;
  }

  public getProducts(): IProduct[] {
    return this.products;
  }

  public addLocation(location: ILocation): void {
    this.locations.push(location);
  }

  public addProduct(product: IProduct): void {
    this.products.push(product);
  }

  public moveLocationUp(index: number): void {
    this.moveElementUp(this.locations, index);
  }

  public moveLocationDown(index: number): void {
    this.moveElementDown(this.locations, index);
  }

  public moveProductUp(index: number): void {
    this.moveElementUp(this.products, index);
  }

  public moveProductDown(index: number): void {
    this.moveElementDown(this.products, index);
  }

  private moveElementUp(array: any[], index: number): void {
    if (index > -1 && array.length > index + 2) {
      const item = array[index];
      array[index] = array[index + 1];
      array[index + 1] = item;
    }
  }

  public moveElementDown(array: any[], index: number): void {
    if (index > 0 && this.locations.length > index + 1) {
      const item = array[index];
      array[index] = array[index - 1];
      array[index - 1] = item;
    }
  }

  public getShoppingList(): any[] {
    return [];
  }
}
