import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ShoppingComponent} from './shopping.component';
import {ShoppingRoutingModule} from './shopping-routing.module';

import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    ShoppingComponent
  ],
  imports: [
    RouterModule,
    ShoppingRoutingModule,
    CommonModule
  ]
})
export class ShoppingModule { }
