import {IProduct} from './IProduct';

export interface ILocation {
  product: IProduct;
  quantity: number;
  unit: string;
}
