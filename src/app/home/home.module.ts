import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule,
    HomeRoutingModule
  ]
})
export class HomeModule {
}
