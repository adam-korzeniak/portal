import {Component} from '@angular/core';
import {AuthenticationService} from './_legacy/auth/service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'My Portal';
  inProgressExpanded = false;
  movieDropdownExpanded = false;

  constructor(private authService: AuthenticationService) {
  }

  public logout(): void {
    this.authService.logout();
    location.reload();
  }

  public isLogged(): boolean {
    return this.authService.isAuthorised();
  }
}
