import {ICryptoHolding} from './ICryptoHolding';

export interface ICryptoHoldings {
  currency: string;
  totalValue: number;
  assets: ICryptoHolding[];
}
