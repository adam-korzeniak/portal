import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';

import {CryptoRoutingModule} from './crypto-routing.module';
import {CryptoComponent} from './crypto/crypto.component';


@NgModule({
  declarations: [
    CryptoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CryptoRoutingModule,
    ReactiveFormsModule
  ]
})
export class CryptoModule {
}
