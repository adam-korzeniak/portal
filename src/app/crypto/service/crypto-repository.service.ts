import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ICryptoHoldings} from '../model/ICryptoHoldings';


@Injectable({providedIn: 'root'})
export class CryptoService {
  private readonly CRYPTO_URL = 'http://localhost:8902' + '/api/crypto?currency=PLN';

  constructor(private http: HttpClient) {
  }

  public getCryptoHoldings(): Observable<ICryptoHoldings> {
    return this.http.get<ICryptoHoldings>(this.CRYPTO_URL);
  }
}
