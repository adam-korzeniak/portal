import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CryptoComponent} from './crypto/crypto.component';

const routes: Routes = [
  {
    path: 'crypto',
    component: CryptoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CryptoRoutingModule {
}
