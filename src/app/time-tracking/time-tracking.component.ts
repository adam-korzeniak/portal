import {Component, OnInit} from '@angular/core';
import {Task} from './model/task';

@Component({
  selector: 'app-time-tracking',
  templateUrl: './time-tracking.component.html',
  styleUrls: ['./time-tracking.component.css']
})
export class TimeTrackingComponent implements OnInit {

  public tasks: Task[];

  constructor() {
  }

  ngOnInit(): void {
    this.tasks = [];
    this.tasks.push(
      {
        name: 'xxx'
      },
      {
        name: 'Drugi'
      }
    );
  }

}
