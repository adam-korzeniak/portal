import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-flashcard',
  templateUrl: './flashcard.component.html',
  styleUrls: ['./flashcard.component.css']
})
export class FlashcardComponent implements OnInit {

  public currentCardId = 0;
  public uncovered = false;
  public flashcards: any[];
  public addingCard = false;
  public randomizedOrder = true;
  data = [
    {
      front: 'Adam',
      back: 'Korzeniak'
    },
    {
      front: 'question',
      back: 'answer'
    },
    {
      front: 'XXXXXX',
      back: 'YYYYYYYYYY'
    },
    {
      front: 'hehehehe',
      back: 'dupadupadupas'
    }
  ];

  constructor() {
  }

  ngOnInit(): void {
    this.flashcards = this.data;
    if (this.randomizedOrder) {
      this.flashcards.sort(() => Math.random() - 0.5);
    }
  }

  public toggle(): void {
    this.uncovered = !this.uncovered;
  }

  public nextCard(): void {
    this.uncovered = false;
    this.currentCardId++;
  }

  public previousCard(): void {
    this.uncovered = false;
    this.currentCardId--;
  }

  public displayAddingCardForm(): void {
    this.addingCard = true;
  }
}
