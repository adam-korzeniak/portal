import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FlashcardComponent} from './flashcard.component';
import {CommonModule} from '@angular/common';
import {FlashcardRoutingModule} from './flashcard-routing.module';


@NgModule({
  declarations: [
    FlashcardComponent
  ],
  imports: [
    RouterModule,
    FlashcardRoutingModule,
    CommonModule
  ]
})
export class FlashcardModule {
}
