import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipeComponent} from './recipe.component';
import {IngredientDetailsComponent} from './ingredient/details/ingredient-details.component';

const routes: Routes = [
  {
    path: 'recipe',
    component: RecipeComponent
  },
  {
    path: 'ingredient',
    component: IngredientDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule {
}
