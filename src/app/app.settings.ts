export class AppSettings {
  public static SERVER_URL = 'https://localhost:8443';
  public static AUTH = 'Basic YWRtaW46YWRtaW4=';
  public static IS_PROD_MODE = false;
  public static AUTH_DISABLED = true;
}
